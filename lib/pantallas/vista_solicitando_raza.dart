import 'package:flutter/material.dart';
import 'package:perritos_api/bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:perritos_api/repositorio/repositorio_verificacion.dart';

class VistaSolicitandoRaza extends StatefulWidget {
  const VistaSolicitandoRaza({Key? key}) : super(key: key);

  @override
  State<VistaSolicitandoRaza> createState() => _VistaSolicitandoRazaState();
}

class _VistaSolicitandoRazaState extends State<VistaSolicitandoRaza> {
  late TextEditingController controlador;
  bool razaValida = false;

   @override
  void initState() {
    controlador = TextEditingController();
    controlador.addListener(escuchar);
    super.initState();
  }

  @override
  void dispose() {
    controlador.dispose();
    super.dispose();
  }

  void escuchar() {
    verificarRegistroRaza(controlador.text);
    final razaValida = controlador.text.isNotEmpty;
    setState(() {
      this.razaValida=razaValida;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 30,
        ),
        const Text('Escriba una raza',style: TextStyle(fontSize: 30)),
        const SizedBox(
          height: 30,
        ),
        TextField(controller: controlador),
        const SizedBox(
          height: 30,
        ),
        OutlinedButton( onPressed: razaValida ? () {
          var b = context.read<BlocVerificacion>();
          b.add(RazaRecibida(controlador.text));
        }: null,
        style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.white)),
        child: const Text('Buscar'))
      ],
    );
  }
}