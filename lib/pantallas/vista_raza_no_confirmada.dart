import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:perritos_api/bloc/bloc.dart';

class VistaRazaNoConfirmada extends StatelessWidget {
  final String raza;
  const VistaRazaNoConfirmada(this.raza,{Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final b = context.read<BlocVerificacion>();
    return Center(
      child: Column(
        children: [
          const SizedBox(
            height: 30,
          ),
          Text('Lo sentimos, la raza $raza no está registrada', style: const TextStyle(fontSize: 30)),
          const SizedBox(
              height: 30,
          ),
          OutlinedButton(onPressed: (){
            b.add(Creado());
          }, 
          style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.white)),
          child: const Text('Volver a intentar'))
        ],
      ),
    );
  }
}