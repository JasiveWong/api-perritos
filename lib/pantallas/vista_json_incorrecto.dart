import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/bloc.dart';

class VistaJsonIncorrecto extends StatelessWidget {
  const VistaJsonIncorrecto({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final b = context.read<BlocVerificacion>();
    return Center(
      child: Column(
        children: [
          const SizedBox(
              height: 30,
          ),
          const Text("Error, Json incorrecto",style: TextStyle(fontSize: 30)),
          const SizedBox(
                height: 30,
            ),
            OutlinedButton(onPressed: (){
              b.add(Creado());
            }, 
            style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.white)),
            child: const Text('Volver a intentar'))
        ],
      ),
    );
  }
}