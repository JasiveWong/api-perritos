// ignore_for_file: prefer_interpolation_to_compose_strings

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:perritos_api/bloc/bloc.dart';

class VistaRazaConfirmada extends StatelessWidget {
  final String raza;
  final String imagen;
  final List subrazas;
  final List imagenesSubrazas;
  const VistaRazaConfirmada(this.raza, this.imagen, this.subrazas, this.imagenesSubrazas,{Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final b = context.read<BlocVerificacion>();
    return Center(
      child: Column(
        children: [
          Text(raza, style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 30)),
          const SizedBox(
              height: 30,
          ),
          Text("Url: $imagen"),
          if(subrazas.isNotEmpty)...[
            Column(
            children: [
            const Text('Subrazas:', style: TextStyle(fontWeight: FontWeight.bold)),
            for (var i = 0; i < subrazas.length; i++) ...[
                Text(subrazas[i]),
                Text('Url: '+imagenesSubrazas[i]),
              ],
            ]
            ),
          ],
          const SizedBox(
              height: 30,
          ),
          OutlinedButton(onPressed: (){
              b.add(Creado());
            }, 
            style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.white)),
            child: const Text('Volver a intentar'))
        ],
      ),
    );
  }
}