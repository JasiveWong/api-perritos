import 'package:flutter/material.dart';

class VistaEsperando extends StatelessWidget {
  const VistaEsperando({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        // ignore: prefer_const_literals_to_create_immutables
        children: [
          const Text('Un momento por favor...', style: TextStyle(fontSize: 40)),
          const SizedBox(
              height: 30,
          ),
          const SizedBox(height: 100.0, width: 100.0,child: CircularProgressIndicator(),),
        ],
      ),
    );
  }
}