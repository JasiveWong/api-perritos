import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:perritos_api/bloc/bloc.dart';
import 'package:perritos_api/pantallas/vista_esperando.dart';
import 'package:perritos_api/pantallas/vista_json_incorrecto.dart';
import 'package:perritos_api/pantallas/vista_raza_confirmada.dart';
import 'package:perritos_api/pantallas/vista_raza_no_confirmada.dart';
import 'package:perritos_api/pantallas/vista_solicitando_raza.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) {
        BlocVerificacion blocVerificacion = BlocVerificacion();
        Future.delayed(const Duration(seconds: 2),(){
          blocVerificacion.add(Creado());
        });
        return blocVerificacion;
      },
      child: const Aplicacion(),
    );
  }
}

class Aplicacion extends StatelessWidget {
  const Aplicacion({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Builder( builder: (context) {
          var estado = context.watch<BlocVerificacion>().state;
          if(estado is Creandose){
            return const Center(child: VistaEsperando());
          }
          if(estado is SolicitandoRaza){
            return const VistaSolicitandoRaza();
          }
          if(estado is EsperandoConfirmacionRaza){
            return const Center(child: VistaEsperando());
          }
          if(estado is MostrandoRazaNoConfirmada){
            return VistaRazaNoConfirmada(estado.raza);
          }
          if(estado is MostrandoRazaConfirmada){
            return VistaRazaConfirmada(estado.raza, estado.imagen, estado.subrazas, estado.imagenesSubrazas);
          }
          if(estado is MostrandoJsonIncorrecto){
            return const VistaJsonIncorrecto();
          }
          return const Center(
            child: Text('Algo salio mal'),
          );
        }),
      ),
    );
  }
}

