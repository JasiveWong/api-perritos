import 'package:bloc/bloc.dart';
import 'package:perritos_api/repositorio/repositorio_verificacion.dart';

class Evento{}

class Creado extends Evento {}

class RazaRecibida extends Evento {
  final String raza;
  RazaRecibida(this.raza); 
}

class RazaConfirmada extends Evento {}
 
class Estado{}

class Creandose extends Estado{}

class SolicitandoRaza extends Estado {}

class EsperandoConfirmacionRaza extends Estado {}

class MostrandoRazaNoConfirmada extends Estado {
  final String raza;
  MostrandoRazaNoConfirmada(this.raza);
}

class MostrandoJsonIncorrecto extends Estado {
  MostrandoJsonIncorrecto();
}

class MostrandoRazaConfirmada extends Estado {
  final String raza;
  final String imagen;
  final List subrazas;
  final List imagenesSubrazas;
  MostrandoRazaConfirmada(this.raza,this.imagen, this.subrazas, this.imagenesSubrazas);

}

class BlocVerificacion extends Bloc<Evento, Estado> {
  BlocVerificacion() : super(Creandose()){
    on<Creado>((event, emit) {
      emit(SolicitandoRaza());
    });
    on<RazaRecibida>((event, emit){
    var resultadoRaza = verificarRegistroRaza(event.raza);
    emit(EsperandoConfirmacionRaza());
        if(resultadoRaza=="Raza no existe... por ahora"||resultadoRaza=='inserte una raza para buscar'){
          emit(MostrandoRazaNoConfirmada(event.raza));
        }
        else if(resultadoRaza=="Json incorrecto"){
          emit(MostrandoJsonIncorrecto());
        }
        else{
          var listaSubrazas = verificarRegistroSubraza(event.raza);
          var listaImagenesSubrazas = obtenerImagenesSubrazas(event.raza);
          emit(MostrandoRazaConfirmada(event.raza, resultadoRaza, listaSubrazas, listaImagenesSubrazas));
        }
    });
  }
}
