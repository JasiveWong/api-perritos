class GetSubraza {
  final List message;

  GetSubraza(this.message);

  GetSubraza.fromJson(Map<String, dynamic> json) :
      message = json['message'];

  Map<String, dynamic> toJson() =>
    {
      'message': message,
    };
}