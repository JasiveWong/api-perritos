import 'dart:convert';
import 'package:perritos_api/repositorio/get.dart';
import 'package:perritos_api/repositorio/get_subraza.dart';

const String _hound = """{
    "message": "https://images.dog.ceo/breeds/hound-english/n02089973_2322.jpg",
    "status": "success"
}""";

const String _houndSubrazas ="""{
    "message": [
        "afghan",
        "basset",
        "blood",
        "english",
        "ibizan",
        "plott",
        "walker"
    ],
    "status": "success"
}""";

const String _afghan="""{"message": "https://images.dog.ceo/breeds/hound-afghan/n02088094_8631.jpg", "status": "success"}""";

const String _basset="""{"message": "https://images.dog.ceo/breeds/hound-basset/n02088238_9292.jpg", "status": "success"}""";

const String _blood="""{"message": "https://images.dog.ceo/breeds/hound-blood/n02088466_8842.jpg", "status": "success"}""";

const String _english="""{"message": "https://images.dog.ceo/breeds/hound-english/n02089973_3401.jpg", "status": "success"}""";

const String _ibizan="""{"message": "https://images.dog.ceo/breeds/hound-ibizan/n02091244_596.jpg", "status": "success"}""";

const String _plott="""{"message": "https://images.dog.ceo/breeds/hound-plott/hhh_plott002.JPG", "status": "success"}""";

const String _walker="""{"message": "https://images.dog.ceo/breeds/hound-walker/n02089867_2063.jpg", "status": "success"}""";

const String _husky = """{"message": "https://images.dog.ceo/breeds/husky/n02110185_6850.jpg", "status": "success"}""";

const String _incorrecto = """{"mepssage": https://images.dog.ceo/breeds/husky/n02110185_6850.jpg", "status: "success}""";

const String _huskySubrazas = """{"message": [], "status": "success"}""";

String _obtenerJsonRaza(String raza){
  if(raza=='husky'){
    return _husky;
  }
  if(raza=='hound'){
    return _hound;
  }
  if(raza=='incorrecto'){
    return _incorrecto;
  }
  return "Raza no existe... por ahora";
}

String _obtenerJsonSubRaza(String raza){
  if(raza=='husky'){
    return _huskySubrazas;
  }
  if(raza=='hound'){
    return _houndSubrazas;
  }
  if(raza=='incorrecto'){
    return _incorrecto;
  }
  return "Raza no existe... por ahora";
}

String _obtenerJsonFotosSubRazas(String subraza){
  if(subraza=='afghan'){
    return _afghan;
  }
  if(subraza=='basset'){
    return _basset;
  }
  if(subraza=='blood'){
    return _blood;
  }
  if(subraza=='english'){
    return _english;
  }
  if(subraza=='ibizan'){
    return _ibizan;
  }
  if(subraza=='plott'){
    return _plott;
  }
  if(subraza=='walker'){
    return _walker;
  }
  if(subraza=='incorrecto'){
    return _incorrecto;
  }
  return "Fotos de subrazas no existen... por ahora";
}
  
String verificarRegistroRaza(String raza){
  if(raza.isNotEmpty){
    var json = _obtenerJsonRaza(raza);
    if(json=="Raza no existe... por ahora"){
      return json;
    }
    try {
      Map<String,dynamic> razaMap = jsonDecode(json);
      var resp= Get.fromJson(razaMap);
      return resp.message.toString(); 
    } catch (e) {
      return "Json incorrecto";
    }
  }else{
    return "inserte una raza para buscar";
  }
}

List verificarRegistroSubraza(String raza) {
  if(raza.isNotEmpty){
    try {
      Map<String,dynamic> subRazaMap = jsonDecode(_obtenerJsonSubRaza(raza));
      var resp = GetSubraza.fromJson(subRazaMap);
      return resp.message;
    } catch (e) {
      return [];
    }
  }
  return [];
}

List obtenerImagenesSubrazas(String raza){
  var listaImagenesSubrazas= [];
  var listaSubrazas = verificarRegistroSubraza(raza);
  if(listaSubrazas.isNotEmpty){
    for(var i=0; i<listaSubrazas.length; i++){
      try {
        Map<String,dynamic> subrazaMap = jsonDecode(_obtenerJsonFotosSubRazas(listaSubrazas[i]));
        var resp = Get.fromJson(subrazaMap);
        listaImagenesSubrazas.add(resp.message);
      } catch (e) {
        return [];
      }
    }
    return listaImagenesSubrazas;
  }else{
    return [];
  }
}
