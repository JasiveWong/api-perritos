class Get {
  final String message;

  Get(this.message);

  Get.fromJson(Map<String, dynamic> json) :
      message = json['message'];

  Map<String, dynamic> toJson() =>
    {
      'message': message,
    };
}