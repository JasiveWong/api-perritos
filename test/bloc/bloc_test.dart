import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:perritos_api/bloc/bloc.dart';

void main() {
  blocTest<BlocVerificacion, Estado>(
    'Cuando se añade evento creado, solicita raza',
    build: () => BlocVerificacion(),
    act: (bloc) => bloc.add(Creado()),
    expect: () => [isA<SolicitandoRaza>()],
  );
  blocTest<BlocVerificacion, Estado>(
    'Cuando raza es husky debo tener MostrandoRazaConfirmada.',
    build: () => BlocVerificacion(),
    seed: () => SolicitandoRaza(),
    act: (bloc) => bloc.add(RazaRecibida('husky')),
    expect: () => [isA<EsperandoConfirmacionRaza>(),
                  isA<MostrandoRazaConfirmada>()],
  );

  blocTest<BlocVerificacion, Estado>(
    'Cuando raza es jasive debo tener MostrandoRazaNoConfirmada.',
    build: () => BlocVerificacion(),
    seed: () => SolicitandoRaza(),
    act: (bloc) => bloc.add(RazaRecibida('jasive')),
    expect: () => [isA<EsperandoConfirmacionRaza>(),
                  isA<MostrandoRazaNoConfirmada>()],
  );

  blocTest<BlocVerificacion, Estado>(
    'Cuando raza es '' debo tener MostrandoRazaNoConfirmada.',
    build: () => BlocVerificacion(),
    seed: () => SolicitandoRaza(),
    act: (bloc) => bloc.add(RazaRecibida('')),
    expect: () => [isA<EsperandoConfirmacionRaza>(),
                  isA<MostrandoRazaNoConfirmada>()],
  );

  blocTest<BlocVerificacion, Estado>(
    'Cuando raza es incorrecto debo tener MostrandoJsonIncorrecto.',
    build: () => BlocVerificacion(),
    seed: () => SolicitandoRaza(),
    act: (bloc) => bloc.add(RazaRecibida('incorrecto')),
    expect: () => [isA<EsperandoConfirmacionRaza>(),
                  isA<MostrandoJsonIncorrecto>()],
  );
}