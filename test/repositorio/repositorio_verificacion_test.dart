import 'package:flutter_test/flutter_test.dart';
import 'package:perritos_api/repositorio/repositorio_verificacion.dart';

void main() {
  test('Con '' me regresa error', ()  {
    var resultado =  verificarRegistroRaza('');
    expect(resultado, equals('inserte una raza para buscar'));
  });
  test('Con husky me regresa imagen', ()  {
    var json =  verificarRegistroRaza('husky');
    var resultado= json.contains('https://images.dog.ceo/breeds/husky');
    expect(resultado, true);
  });

  test('Con incorrecto me regresa error', ()  {
    var json =  verificarRegistroRaza('incorrecto');
    expect(json, "Json incorrecto");
  });

  test('Con husky me regresa vacio', ()  {
    var resultado =  verificarRegistroSubraza('husky');
    expect(resultado, equals([]));
  });
  test('Con hound me regresa subrazas', ()  {
    var resultado =  verificarRegistroSubraza('hound');
    expect(resultado, equals(['afghan', 'basset', 'blood', 'english', 'ibizan', 'plott', 'walker']));
  });

  test('Con incorrecto me regresa vacio', ()  {
    var resultado =  verificarRegistroSubraza('incorrecto');
    expect(resultado, equals([]));
  });

  test('Con raza husky me regresa vacio', ()  {
    var resultado =  obtenerImagenesSubrazas('husky');
    expect(resultado, equals([]));
  });
  test('Con raza hound me regresa imagen', ()  {
    var resultado = obtenerImagenesSubrazas('hound');
    List lista = [
      "https://images.dog.ceo/breeds/hound-afghan/n02088094_8631.jpg",
      "https://images.dog.ceo/breeds/hound-basset/n02088238_9292.jpg",
      "https://images.dog.ceo/breeds/hound-blood/n02088466_8842.jpg",
      "https://images.dog.ceo/breeds/hound-english/n02089973_3401.jpg",
      "https://images.dog.ceo/breeds/hound-ibizan/n02091244_596.jpg",
      "https://images.dog.ceo/breeds/hound-plott/hhh_plott002.JPG",
      "https://images.dog.ceo/breeds/hound-walker/n02089867_2063.jpg"
    ];
    expect(resultado, lista);
  });

  test('Con raza incorrecto me regresa vacio', ()  {
    var resultado =  obtenerImagenesSubrazas('incorrecto');
    expect(resultado, equals([]));
  });
}